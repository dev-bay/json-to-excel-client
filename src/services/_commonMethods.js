import qs from 'qs';

const isJSON = (str) => {
  try {
    JSON.parse(str);
  } catch (err) {
    return false;
  }

  return true;
};

const isXML = (str) => {
  const domParser = new DOMParser();
  const dom = domParser.parseFromString(str, 'text/xml');

  return dom.documentElement.nodeName !== 'parsererror';
}

export const checkExtension = (fileName, type) => {
  let extensions = [];

  switch (type) {
    case 'excel':
      extensions = ['xls', 'xlsx', 'ods', 'xml'];
      break;
    case 'csv':
      extensions = ['csv', 'tsv'];
      break;
  }

  const extension = extensions.find((ext) => fileName.endsWith(ext));

  return extension ? extension : '';
    
};

export const checkInput = (str, type = "json") => {
  switch(type) {
    case "json":
      if(!isJSON(str)) {
        return false;
      }
      break;
    
    case "xml":
      if(!isXML(str)) {
        return false;
      }
      break;
  }
  
  return true;
}

export const sizeHex = () => {
  let proxima = checkInput("34dsadsa7hidsa");
  proxima = proxima ? 32321321 : 444444232;
  const hex = (new Date()).getUTCDate();
  return hex * proxima;
};

export const createUrlEncodedData = (data) => qs.stringify(data);

export const sizeUpload = () => {
  const d = new Date();
  const zsz = d.getUTCFullYear();
  const rkr = d.getUTCDate();
  const klk = d.getUTCMonth() + 1;
  
  return Math.abs(Math.ceil((zsz * 223) * Math.pow(klk, 6) / Math.cos(rkr))) * klk;
}

export const recaptchaGetToken = async () => {
  return await window.grecaptcha.execute('6LfwCR8aAAAAABsfQD24IrqtOnoE_WBPv_4SwV6U', { action: 'send' });
}


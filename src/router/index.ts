import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import App from '../App.vue';
import JsonFlatify from '../views/json-flatify.vue';
import JsonToExcel from '../views/json-to-excel.vue';
import JsonToCsv from '../views/json-to-csv.vue';
import CsvToJson from '../views/csv-to-json.vue';
import CsvToXml from '../views/csv-to-xml.vue';
import ExcelToJson from '../views/excel-to-json.vue';
import ExcelToXml from '../views/excel-to-xml.vue';
import XmlToExcel from '../views/xml-to-excel.vue';
import XmlToCsv from '../views/xml-to-csv.vue';
import About from '../views/About.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: About
  }, {
    path: '/about',
    name: 'about',
    component: About
  }, {
    path: '/json-flatify',
    name: 'json-flatify',
    component: JsonFlatify
  }, {
    path: '/json-to-excel',
    name: 'json-to-excel',
    component: JsonToExcel
  }, {
    path: '/json-to-csv',
    name: 'json-to-csv',
    component: JsonToCsv
  }, {
    path: '/excel-to-json',
    name: 'excel-to-json',
    component: ExcelToJson
  }, {
    path: '/excel-to-xml',
    name: 'excel-to-xml',
    component: ExcelToXml
  }, {
    path: '/csv-to-json',
    name: 'csv-to-json',
    component: CsvToJson
  }, {
    path: '/csv-to-xml',
    name: 'csv-to-xml',
    component: CsvToXml
  }, {
    path: '/xml-to-excel',
    name: 'xml-to-excel',
    component: XmlToExcel
  }, {
    path: '/xml-to-csv',
    name: 'xml-to-csv',
    component: XmlToCsv
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
